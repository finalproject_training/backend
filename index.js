//add node.js modul
const express = require('express')
var cors = require('cors')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
var mongoose = require('mongoose')
var fs = require('fs')
const path = require('path')
const { authenMiddleware, authorizeMiddleware } = require('./helpers/auth')

mongoose.connect('mongodb://localhost:27017/train', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

//add router in here
const trainPostRouter = require('./routes/train')
const studentRouter = require('./routes/student')
const userRouter = require('./routes/user')
const authRouter = require('./routes/auth')

//check connect database
var db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))
db.once('open', function () {
  console.log('connectDB')
})

const dotenv = require('dotenv')
const { ROLE } = require('./constant/ROLE')

// get config vars
dotenv.config()

//use modul
const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

//add API in here
app.use(
  '/trains',
  authenMiddleware,
  authorizeMiddleware([ROLE.ADMIN, ROLE.STUDENT]),
  trainPostRouter
)
app.use(
  '/students',
  authenMiddleware,
  authorizeMiddleware([ROLE.ADMIN, ROLE.STUDENT]),
  studentRouter
)
app.use(
  '/users',
  authenMiddleware,
  authorizeMiddleware([ROLE.ADMIN]),
  userRouter
)
app.use('/auth', authRouter)

module.exports = app
