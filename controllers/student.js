const student = require('../models/student')
const studentController = {
  async add (req, res, next) {
    const payload = req.body
    const students = new student(payload)
    try {
      await students.save()
      res.json(students)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async update (req, res, next) {
    const payload = req.body
    try {
      const students = await student.updateOne({ _id: payload._id }, payload)
      res.json(students)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async delete (req, res, next) {
    const { id } = req.params
    try {
      const students = await student.delete({ _id: id })
      res.json(students)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async get (req, res, next) {
    try {
      const students = await student.find({})
      res.json(students)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getID (req, res, next) {
    const { id } = req.params
    try {
      const students = await student.findById(id)
      res.json(students)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getOne (req, res, next) {
    const studentCode = req.body.studentCode
    try {
      const students = await student.findOne({ studentCode: studentCode })
      res.json(students)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = studentController
