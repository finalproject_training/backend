const user = require('../models/user')
const userController = {
  async add (req, res) {
    const payload = req.body
    const users = new user(payload)
    try {
      await users.save()
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async update (req, res) {
    const payload = req.body
    try {
      const users = await user.updateOne({ _id: payload._id }, payload)
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async delete (req, res) {
    const { id } = req.params
    console.log(id)
    try {
      const users = await user.deleteOne({ _id: id })
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async get (req, res) {
    try {
      const users = await user.find({}).exec()
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getID (req, res) {
    const { id } = req.params
    try {
      const users = await user.findById(id).exec()
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = userController
