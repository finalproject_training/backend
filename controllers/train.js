const train = require('../models/train')
const trainController = {
  async add (req, res) {
    const payload = req.body
    const trains = new train(payload)
    try {
      await trains.save()
      res.json(trains)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async update (req, res) {
    const payload = req.body
    console.log(payload)
    try {
      const trains = await train.updateOne({ _id: payload._id }, payload)
      res.json(trains)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async delete (req, res) {
    const { id } = req.params
    console.log(id)
    try {
      const trains = await train.deleteOne({ _id: id })
      console.log('del success')
      res.json(trains)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async get (req, res) {
    try {
      const trains = await train.find({}).sort('-datePost')
      res.json(trains)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getID (req, res) {
    const { id } = req.params
    try {
      const trains = await train.findById(id)
      res.json(trains)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getOne (req, res, next) {
    const title = req.body.title
    try {
      const trains = await train.findOne({ title: title })
      res.json(trains)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = trainController
