const express = require('express')
const router = express.Router()
const studentController = require('../controllers/student')

router.get('/', studentController.get)
router.get('/:id', studentController.getID)
router.post('/add', studentController.add)
router.put('/update', studentController.update)
router.delete('/del/:id', studentController.delete)
router.post('/', studentController.getOne)  

module.exports = router
