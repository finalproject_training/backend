const express = require('express')
const router = express.Router()
const userController = require('../controllers/user')

router.get('/', userController.get)
router.get('/:id', userController.getID)
router.post('/add', userController.add)
router.put('/update', userController.update)
router.delete('/del/:id', userController.delete)

module.exports = router
