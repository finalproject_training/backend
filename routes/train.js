const express = require('express')
const router = express.Router()
const trainController = require('../controllers/train')

router.get('/', trainController.get)
router.get('/:id', trainController.getID)
router.post('/add', trainController.add)
router.put('/update', trainController.update)
router.delete('/del/:id', trainController.delete)
router.post('/', trainController.getOne)

module.exports = router
