const mongoose = require('mongoose')
const { PROGRAME } = require('../constant/PROGRAME')
const { CLASS } = require('../constant/CLASS')

const trainSchema = new mongoose.Schema({
  title: String,
  description: String,
  conditions: {
    programe: {
      type: [String],
      default: [PROGRAME.CS]
    },
    class: {
      type: [String],
      default: [CLASS.ONE]
    }
  },
  narrator: String,
  channel: String,
  dateTrain: String,
  amount: String,
  members: [
    {
      _id: String,
      studentCode: String,
      name: String,
      programe: {
        type: [String],
        default: [PROGRAME.CS]
      },
      class: {
        type: [String],
        default: [CLASS.ONE]
      }
    }
  ],
  queues: [
    {
      _id: String,
      studentCode: String,
      name: String,
      programe: {
        type: [String],
        default: [PROGRAME.CS]
      },
      class: {
        type: [String],
        default: [CLASS.ONE]
      },
      dateRegis: {
        type: Date,
        default: Date.now
      }
    }
  ],
  datePost: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('train', trainSchema)
