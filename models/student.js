const mongoose = require('mongoose')
const { PROGRAME } = require('../constant/PROGRAME')
const { CLASS } = require('../constant/CLASS')

const studentSchema = new mongoose.Schema({
  studentCode: String,
  name: String,
  programe: {
    type: [String],
    default: [PROGRAME.CS]
  },
  class: {
    type: [String],
    default: [CLASS.ONE]
  },
  trainHistories: [
    {
      trainPost: String,
      title: String,
      dateTrain: String,
      status: String
    }
  ]
})

module.exports = mongoose.model('student', studentSchema)
