const jwt = require('jsonwebtoken')
const User = require('../models/user')

const generateAccessToken = function (user) {
  return jwt.sign(user, process.env.TOKEN_SECRET, { expiresIn: '86400s' })
}

const authenMiddleware = function (req, res, next) {
  const authHeader = req.headers.authorization
  const token = authHeader && authHeader.split(' ')[1]

  console.log(token)

  if (token === null) return res.sendStatus(401)
  jwt.verify(token, process.env.TOKEN_SECRET, async function (err, user) {
    console.log(err)
    if (err) return res.sendStatus(403)
    const currentUser = await User.findById(user._id).exec()
    req.user = currentUser
    console.log(user)
    next()
  })
}

const authorizeMiddleware = function (roles) {
  return function (req, res, next) {
    console.log(req.user.roles)
    for (let i = 0; i < roles.length; i++) {
      if (req.user.roles.indexOf(roles[i]) >= 0) {
        next()
        return
      }
    }
  }
}

module.exports = {
  generateAccessToken,
  authenMiddleware,
  authorizeMiddleware
}
