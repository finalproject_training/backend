const ROLE = {
  ADMIN: 'ADMIN',
  STUDENT: 'STUDENT'
}

module.exports = { ROLE }
