const mongoose = require('mongoose')
const Train = require('../models/train')
const { PROGRAME } = require('../constant/PROGRAME')
const { CLASS } = require('../constant/CLASS')

mongoose.connect('mongodb://localhost:27017/train')

async function clearTrain () {
  await Train.deleteMany({})
}
async function main () {
  await clearTrain()

  const train1 = new Train({
    title: 'การเขียน RESUME',
    description: 'การอบรมแนะนำแขวทางการเขียน RESUME',
    conditions: {
      programe: [PROGRAME.CS, PROGRAME.IT],
      class: [CLASS.FOUR]
    },
    narrator: 'บริษัท CLICK NEXT',
    channel: 'Googlemeet.com',
    dateTrain: '30 พ.ค. 2565',
    amount: '200'
  })
  train1.save()

  const train2 = new Train({
    title: 'โครงการ Python Coding Camp+',
    description: 'โครงการสำหรับนักศึกษาใหม่ เพื่อเป็นพื้นฐานของการเขียนโปรแกรม',
    conditions: {
      programe: [PROGRAME.CS, PROGRAME.IT, PROGRAME.SE, PROGRAME.AI],
      class: [CLASS.ONE]
    },
    narrator: 'InFormaticsBUU',
    channel: 'Googlemeet.com',
    dateTrain: '31 พ.ค. 2565',
    amount: '70'
  })
  train2.save()

  const train3 = new Train({
    title: 'อบรมการเตรียมตัวอบรมสมัครงาน',
    description: 'โครงการสำหรับนักศึกษาใหม่ เพื่อเป็นพื้นฐานของการเขียนโปรแกรม',
    conditions: {
      programe: [PROGRAME.CS, PROGRAME.IT, PROGRAME.SE],
      class: [CLASS.FOUR]
    },
    narrator: 'SCB',
    channel: 'ZOOM.com',
    dateTrain: '15 พ.ค. 2565',
    amount: '70'
  })
  train3.save()

  const train4 = new Train({
    title: 'NO SQL',
    description: 'การอบรมเกี่ยวกับ MongoDB',
    conditions: {
      programe: [PROGRAME.CS],
      class: [CLASS.TWO, CLASS.THREE]
    },
    narrator: 'InFormaticsBUU',
    channel: 'Googlemeet.com',
    dateTrain: '18 พ.ค. 2565',
    amount: '70'
  })
  train4.save()

  const train5 = new Train({
    title: 'การออกแบบ UI',
    description: 'การอบรมแนะนำแขวทางการออกแบบ UI',
    conditions: {
      programe: [PROGRAME.CS, PROGRAME.IT],
      class: [CLASS.TWO, CLASS.THREE]
    },
    narrator: 'บริษัท CLICK NEXT',
    channel: 'Googlemeet.com',
    dateTrain: '7 มิ.ย. 2565',
    amount: '70'
  })
  train5.save()

  const train6 = new Train({
    title: 'การพัฒนาเว็บไซต์ขั้นพื้นฐาน',
    description: 'โครงการสำหรับนักศึกษาใหม่ เพื่อเป็นพื้นฐานของการเขียนโปรแกรม',
    conditions: {
      programe: [PROGRAME.CS, PROGRAME.IT, PROGRAME.SE],
      class: [CLASS.TWO]
    },
    narrator: 'InFormaticsBUU',
    channel: 'Googlemeet.com',
    dateTrain: '10 มิ.ย. 2565',
    amount: '70'
  })
  train6.save()
}

main().then(function () {
  console.log('finish')
})
