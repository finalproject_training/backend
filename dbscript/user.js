const mongoose = require('mongoose')
const User = require('../models/user')
const { ROLE } = require('../constant/ROLE')
mongoose.connect('mongodb://localhost:27017/train')
async function clearUser () {
  await User.deleteMany({})
}
async function main () {
  await clearUser()

  const student = new User({
    username: '61160050@go.buu.ac.th',
    password: 'password',
    roles: [ROLE.STUDENT]
  })
  student.save()

  const admin = new User({
    username: 'admin@go.buu.ac.th',
    password: 'password',
    roles: [ROLE.ADMIN, ROLE.STUDENT]
  })
  admin.save()
}

main().then(function () {
  console.log('finish')
})
