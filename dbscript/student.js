const mongoose = require('mongoose')
const Student = require('../models/student')
const { PROGRAME } = require('../constant/PROGRAME')
const { CLASS } = require('../constant/CLASS')

mongoose.connect('mongodb://localhost:27017/train')

async function clearStudent () {
  await Student.deleteMany({})
}
async function main () {
  await clearStudent()

  const student = new Student({
    studentCode: '61160050',
    name: 'Issarat Jaijaroon',
    programe: [PROGRAME.CS],
    class: [CLASS.FOUR]
  })
  student.save()
}

main().then(function () {
  console.log('finish')
})
